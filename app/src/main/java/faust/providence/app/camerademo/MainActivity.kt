package faust.providence.app.camerademo

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Button
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    companion object {
        val ACTION_CAMERA_REQUEST_CODE = 1
        val HERE = "here"
        val PHOTO = "data"
        private var bm: Bitmap? = null
        private var location: LatLng = LatLng(0.0,0.0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener{
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, MainActivity.ACTION_CAMERA_REQUEST_CODE)
            }
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        askGPS()

        if(MainActivity.bm != null) {
            this.imageView.setImageBitmap(MainActivity.bm)
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun takePhoto(view: View) {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, MainActivity.ACTION_CAMERA_REQUEST_CODE)
        }
    }

    /**
     * Show map with here
     */
    @Suppress("UNUSED_PARAMETER")
    fun showMap(view: View) {
        val intent = Intent(this, MapsActivity::class.java)
        intent.putExtra(HERE, MainActivity.location)
        this.startActivity(intent)
    }

    /**
     * Return from child (camera)
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            ACTION_CAMERA_REQUEST_CODE -> {
                if(resultCode == Activity.RESULT_OK && data != null && data.hasExtra(PHOTO) ) {
                    // extras?, the question mark is used to depress the warning:
                    //          Unsafe use of a nullable receiver of type Bundle?
                    MainActivity.bm = data.extras?.get(PHOTO) as Bitmap
                    if (MainActivity.bm != null) {
                        this.imageView.setImageBitmap(MainActivity.bm)
                    }
                }

            }
        }
    }

    /**
     * Get the last location
     */
    fun askGPS() {
        try {
            // latitude 緯度, longitude 經度
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        MainActivity.location = LatLng(location.latitude, location.longitude)
                        txtGPS.text = "GPS: (" + MainActivity.location.toString() + " )"
                    }
                }
        } catch(ex: SecurityException) {
            Log.d("myTag", "Security Exception, no location available");
        }
    }
}
